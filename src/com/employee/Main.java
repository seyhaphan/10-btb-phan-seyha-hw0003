package com.employee;

import java.util.*;

public class Main {
    static Scanner sc = new Scanner(System.in);
    int id,hoursWorked;
    String name,address;
    double rate,salary,bonus;
    List<StaffMember> lists = new ArrayList<>();
    Object objs[] = null;

    void addEmployee(StaffMember staffMember){
        lists.add(staffMember);
    }
    void showEmployee(){
        objs = lists.toArray();
        Arrays.sort(objs,new NameSort());

        for (Object obj : objs){
            System.out.println();
            System.out.println(obj.toString());
            System.out.println("-----------------------------------------------");
        }
    }
    boolean isAvailableID(int id){
        for (int i =0; i<objs.length; i++){
            if ( ((StaffMember) objs[i]).getId() == id ){
                System.out.println("Staff Member's ID is already!");
                return false;
            }
        }
        return true;
    }
    void line(String title){
        System.out.println("================= "+ title +" =================");
    }
    void init(){
        StaffMember staffMember1 = new Volunteer(1,"Chan Sok","Kompong Cham");
        StaffMember staffMember2 = new HourlyEmployee(2,"Sok Sombo","Phnom Penh",0,100);
        StaffMember staffMember3 = new SalariedEmployee(3,"Anny Bona","Kompong Speu",500,100);
        addEmployee(staffMember1);
        addEmployee(staffMember2);
        addEmployee(staffMember3);
    }
    void viewEmployee(){
        init();
        showEmployee();
    }
    void addVolunteerEmp(){
        line("INSERT INFO");
        id = Validator.getInteger(sc,"=> Enter Staff Member's ID  : ");
        if (isAvailableID(id)){
            name = Validator.getName(sc,"=> Enter Staff Member's Name  : ");
            address = Validator.getString(sc,"=> Enter Staff Member's Address  : ");
            StaffMember volunteerEmp = new Volunteer(id,name,address);
            addEmployee(volunteerEmp);
            showEmployee();
        }

    }
    void addHourlyEmp(){
        line("INSERT INFO");
        id = Validator.getInteger(sc,"=> Enter Staff Member's ID  : ");
        if (isAvailableID(id)){
            name = Validator.getName(sc,"=> Enter Staff Member's Name  : ");
            address = Validator.getString(sc,"=> Enter Staff Member's Address  : ");
            hoursWorked = Validator.getInteger(sc,"=> Enter Hours Worked  : ");
            rate = Validator.getDouble(sc,"=> Enter Rate  : ");
            StaffMember hourlyEmp = new HourlyEmployee(id,name,address,hoursWorked,rate);
            addEmployee(hourlyEmp);
            showEmployee();
        }

    }
    void addSalariedEmp(){
        line("INSERT INFO");
        id = Validator.getInteger(sc,"=> Enter Staff Member's ID  : ");
        if (isAvailableID(id)){
            name = Validator.getName(sc,"=> Enter Staff Member's Name  : ");
            address = Validator.getString(sc,"=> Enter Staff Member's Address  : ");
            salary = Validator.getDouble(sc,"=> Enter Staff Salary  : ");
            bonus = Validator.getDouble(sc,"=> Enter Staff Bonus  : ");
            StaffMember salariedEmp  = new SalariedEmployee(id,name,address,salary,bonus);
            addEmployee(salariedEmp);
            showEmployee();
        }
    }
    void addEmployee(){
        int option =0;
        do {
            System.out.println("===============================================");
            System.out.println("1). Volunteer \t 2).Hourly Emp \t 3). Salaried Emp \t 4). Back\n");
            option = Validator.getInteger(sc,"=> Choose option(1-4): ");
            switch (option){
                case 1:
                    addVolunteerEmp();
                    break;
                case 2:
                    addHourlyEmp();
                    break;
                case 3:
                    addSalariedEmp();
                    break;
                case 4:
                    break;
                default:
                    System.out.println("Invalid number!");
                    continue;
            }
        }while (option !=4);
    }
    void deleteEmployee(){
        int id[]  = new int[objs.length];
        for (int i =0; i< objs.length; i++){
            id[i] = ((StaffMember) objs[i]).getId();
        }
        line("DELETE");
        int search = Validator.getInteger(sc,"=> Enter Employee ID to remove : ");
        int found =0;
        for(int i = 0; i< id.length; i++){
            if (search == id[i]){
                found = 1;
                System.out.println(objs[i].toString());
                System.out.println("Remove Successfully");
                lists.remove(objs[i]);
                System.out.println("-------------------------------------");
                showEmployee();
            }
        }
        if (found ==0){
            System.out.println("No Staff Member found! ");
        }
    }
    void editEmployee(){
        int id[]  = new int[objs.length];
        for (int i =0; i< objs.length; i++){
            id[i] = ((StaffMember) objs[i]).getId();
        }
        line("EDIT INFO");
        int search = Validator.getInteger(sc,"=> Enter Employee ID to Update : ");
        for(int i = 0; i< id.length; i++){
            if (search == id[i]){
                System.out.println(objs[i].toString());
                System.out.println("========== NEW INFORMATION OF STAFF MEMBER ==========");

                if((objs[i]).getClass().getSimpleName().equals("Volunteer")){

                    String updateName = Validator.getName(sc,"=> Enter Staff Member's name : ");
                    String updateAddress = Validator.getName(sc,"=> Enter Staff Member's Address : ");
                    ((Volunteer) objs[i]).setName(updateName);
                    ((Volunteer) objs[i]).setAddress(updateAddress);
                }
                else if((objs[i]).getClass().getSimpleName().equals("SalariedEmployee")){

                    String updateName = Validator.getName(sc,"=> Enter Staff Member's name : ");
                    double updateSalary = Validator.getDouble(sc,"=> Enter New Salary : ");
                    double updateBonus = Validator.getDouble(sc,"=> Enter New Bonus : ");
                    ((SalariedEmployee) objs[i]).setName(updateName);
                    ((SalariedEmployee) objs[i]).setSalary(updateSalary);
                    ((SalariedEmployee) objs[i]).setBonus(updateBonus);
                }else{
                    String updateName = Validator.getName(sc,"=> Enter Staff Member's name : ");
                    int updateHoursWorked = Validator.getInteger(sc,"=> Enter New HoursWorked : ");
                    double updateRate = Validator.getDouble(sc,"=> Enter New Rate : ");
                    ((HourlyEmployee) objs[i]).setName(updateName);
                    ((HourlyEmployee) objs[i]).setHoursWorked(updateHoursWorked);
                    ((HourlyEmployee) objs[i]).setRate(updateRate);
                }
                showEmployee();
            }
        }


    }
    public static void main(String[] args) {
        Main main = new Main();
        main.viewEmployee();
        int option =0;
        Exit:
        do {
            System.out.println("===============================================");
            System.out.println("1). Add Employee \t 2).Edit \t 3). Remove \t 4). Exit\n");
            option = Validator.getInteger(sc,"=> Choose option(1-4): ");

            switch (option){
                case 1:
                    main.addEmployee();
                    break;
                case 2:
                    main.editEmployee();
                    break;
                case 3:
                    main.deleteEmployee();
                    break;
                case 4:
                    System.out.println("Good Bye (^_^)");
                    break Exit;
                default:
                    System.out.println("Invalid number!");
            }
        }while (option != 4);
    }
}

//sort name
class NameSort implements Comparator {
    @Override
    public int compare(Object obj1, Object obj2) {
        String name1 = ((StaffMember) obj1).getName();
        String name2 = ((StaffMember) obj2).getName();
        return name1.compareTo(name2);
    }
}