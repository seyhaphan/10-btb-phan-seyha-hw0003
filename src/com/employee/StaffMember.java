package com.employee;

abstract public class StaffMember {
    private int id;
    private String name;
    private String address;
    public StaffMember(){}
    public StaffMember(int id,String name,String address){
        this.setId(id);
        this.setName(name);
        this.setAddress(address);
    }

    abstract public double pay();
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String toString(){
        return "ID : " + getId() +
                "\nName : " + getName() +
                "\nAddress : " + getAddress();
    }
}