package com.employee;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Validator {
    static String namePattern = "[a-zA-Z\\s]+";
    static String addressPattern = "[a-zA-Z0-9\\s]+";
    static boolean isValid(String input,String pattern){
        return Pattern.matches(pattern,input);
    }
    static String getName(Scanner sc,String prompt){
        boolean isValid = false;
        String name="";
        String titleCase[] = null;
        while(isValid == false){
            System.out.print(prompt);
            name = sc.nextLine();
            if (isValid(name,namePattern)){
                if (name.length() > 1){
                    String[] getName = name.split(" ");
                    titleCase = new String[getName.length];
                    for (int i = 0; i< titleCase.length; i++){
                        titleCase[i] = new String(getName[i].substring(0,1).toUpperCase() + getName[i].substring(1).toLowerCase());
                    }
                    isValid = true;
                }
            }else{
                System.out.println("Invalid name!");
            }
        }
        String returnName ="";
        if (titleCase != null){
            for (String s : titleCase){
                returnName += s+" ";
            }
        }
        return returnName;
    }
    static String getString(Scanner sc,String prompt){
        boolean isValid = false;
        String name="";
        String titleCase[] = null;
        while(isValid == false){
            System.out.print(prompt);
            name = sc.nextLine();
            if (isValid(name,addressPattern)){
                if (name.length() > 1){
                    String[] getName = name.split(" ");
                    titleCase = new String[getName.length];
                    for (int i = 0; i< titleCase.length; i++){
                        titleCase[i] = new String(getName[i].substring(0,1).toUpperCase() + getName[i].substring(1).toLowerCase());
                    }
                    isValid = true;
                }
            }else{
                System.out.println("Invalid name!");
            }
        }
        String returnName ="";
        if (titleCase != null){
            for (String s : titleCase){
                returnName += s+" ";
            }
        }
        return returnName;
    }
    static int getInteger(Scanner sc,String prompt){
        boolean isValid = false;
        int number = 0;
        while (isValid == false){
            System.out.print(prompt);
            if (sc.hasNextInt()){
                number = sc.nextInt();
                isValid = true;
            }else{
                System.out.println("Invalid number! ");
            }
            //next input
            sc.nextLine();
        }
        return number;
    }
    static double getDouble(Scanner sc,String prompt){
        boolean isValid = false;
        double number = 0;
        while (isValid == false){
            System.out.print(prompt);
            if (sc.hasNextDouble()){
                number = sc.nextInt();
                isValid = true;
            }else{
                System.out.println("Invalid number! ");
            }
            //next input
            sc.nextLine();
        }
        return number;
    }
}