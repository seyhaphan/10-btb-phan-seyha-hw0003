package com.employee;

public class Volunteer extends StaffMember {
    public Volunteer(){
        super();
    }
    public Volunteer(int id, String name, String address){
        super(id, name, address);
    }
    @Override
    public String toString(){
        return super.toString() + "\nThank!";
    }
    @Override
    public double pay() {
        return 0.0;
    }
}