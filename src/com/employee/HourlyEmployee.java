package com.employee;

public class HourlyEmployee extends StaffMember{
    private double rate;
    private int hoursWorked;
    public HourlyEmployee(){super();}
    public HourlyEmployee(int id,String name,String address,int hoursWorked,double rate){
        super(id, name, address);
        this.setHoursWorked(hoursWorked);
        this.setRate(rate);
    }
    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
    @Override
    public String toString(){
        return super.toString() +
                "\nHours Worked : " + getHoursWorked() +
                "\nRate : " + getRate() +
                "\nPayment : " + pay();
    }
    @Override
    public double pay() {
        double pay =0;
        if (getHoursWorked() > 0 && getRate() > 0){
            pay = getHoursWorked() * getRate();
        }
        return pay;
    }
}