package com.employee;

public class SalariedEmployee extends StaffMember{
    private double salary;
    private double bonus;
    public SalariedEmployee(){
        super();
    }
    public SalariedEmployee(int id,String name,String address,double salary,double bonus){
        super(id, name, address);
        this.setSalary(salary);
        this.setBonus(bonus);
    }
    public double getSalary() {
        return salary;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }
    public double getBonus() {
        return bonus;
    }
    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
    @Override
    public String toString(){
        return super.toString() +"\nSalary : " + getSalary() + "\nBonus : " + getBonus() + "\nPayment : " + pay();
    }
    @Override
    public double pay() {
        double pay =0;
        if (getSalary() > 0 && getBonus() > 0){
            pay = getSalary() + getBonus();
        }
        return pay;
    }

}